
COMPATIBLE_BOARDS = maple_mini

INCLUDES += \
		   $(PROJECT_ROOT)/include

PROJECT_SRC += \
			src/main.c

PLATFORM_SRC += \
				lib/stm32/f1/gpio.c \
				lib/stm32/common/gpio_common_all.c \
				lib/stm32/common/rcc_common_all.c

ELF_LD_FLAGS += \
				-nostdlib -nodefaultlibs
