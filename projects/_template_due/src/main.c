#include <asf.h>
#include <conf_board.h>
#include <conf_clock.h>

#define MY_LED    IOPORT_CREATE_PIN(PIOA, 0)

void SysTick_Handler()
{
    // empty
}

void TC0_Handler()
{
    tc_get_status(TC0, 0);
    ioport_toggle_pin_level(MY_LED);
}

void configure_blinking()
{
    uint32_t div;
    uint32_t tcclks;
    uint32_t sysclk = sysclk_get_cpu_hz();

    ioport_init();
    ioport_set_pin_dir(MY_LED, IOPORT_DIR_OUTPUT);

    pmc_enable_periph_clk(ID_TC0);
    tc_find_mck_divisor(4, sysclk, &div, &tcclks, sysclk);
    tc_init(TC0, 0, tcclks| TC_CMR_CPCTRG);
    tc_write_rc(TC0, 0, (sysclk / div) / 4);

    NVIC_EnableIRQ((IRQn_Type) ID_TC0);
    tc_enable_interrupt(TC0, 0, TC_IER_CPCS);

    tc_start(TC0, 0);
}

int main()
{
    sysclk_init();
    board_init();

    configure_blinking();

    while(1)
    {
    }
    
    return 0;
}
