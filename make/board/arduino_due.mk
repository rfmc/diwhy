# BOARD = arduino_due

VARIANT = sam3x8e

LINKER_SCRIPT = $(PLATFORM_ROOT)/sam/utils/linker_scripts/sam3x/sam3x8/gcc/flash.ld

PLATFORM_SRC += \
				sam/boards/arduino_due_x/init.c

CPPFLAGS += \
       -D BOARD=ARDUINO_DUE_X \
       -D __SAM3X8E__ 
