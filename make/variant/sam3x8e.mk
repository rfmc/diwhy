DIR_ASF = $(DIR_EXT)/xdk-asf-3.32.0
ASF_DIST = $(DIR_DIST)/asf-standalone-archive-3.32.0.48.zip

TARGET = arm-none-eabi
PLATFORM_ROOT = $(DIR_ASF)

CPPFLAGS += -D__sam3x8e__ 
ELF_LD_FLAGS += -Wl,--entry=Reset_Handler -mcpu=cortex-m3 -mthumb
LD = $(LD_CXX)

CPPFLAGS += -mcpu=cortex-m3 -mthumb

PLATFORM_SRC += \
       common/services/clock/sam3x/sysclk.c \
       sam/utils/cmsis/sam3x/source/templates/exceptions.c \
       sam/utils/cmsis/sam3x/source/templates/gcc/startup_sam3x.c \
       sam/utils/cmsis/sam3x/source/templates/system_sam3x.c \
       sam/utils/syscalls/gcc/syscalls.c

all: $(DIR_ASF)

$(DIR_ASF):
	$(Q)unzip $(ASF_DIST) -d $(shell dirname $(DIR_ASF))

