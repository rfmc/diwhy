DIR_OPENCM = $(DIR_EXT)/libopencm3-master
OPENCM_DIST = $(DIR_DIST)/libopencm3-master.zip

TARGET = arm-none-eabi
LLVM_RUNTIME = armv7m_soft_nofp
PLATFORM_ROOT = $(DIR_OPENCM)


# -------------------------------------------------------------------------------
# Linker script handling

DEVICE ?= $(error DEVICE is not defined in board config)

LINKER_SCRIPT_DIR = $(DIR_OUT)/ld
LINKER_SCRIPT = $(LINKER_SCRIPT_DIR)/$(DEVICE).ld
LINKER_SCRIPT_DEVICE_DATA = $(DIR_OPENCM)/ld/devices.data
LINKER_SCRIPT_TEMPLATE = $(DIR_OPENCM)/ld/linker.ld.S
LINKER_SCRIPT_CPPFLAGS = $(shell $(DIR_OPENCM)/scripts/genlink.py $(LINKER_SCRIPT_DEVICE_DATA) $(DEVICE) DEFS)

$(LINKER_SCRIPT_DIR):
	@echo $(MSG_MKDIR)
	$(Q)mkdir -p $@

$(LINKER_SCRIPT): $(LINKER_SCRIPT_TEMPLATE) $(DIR_OPENCM) $(LINKER_SCRIPT_DIR) $(LINKER_SCRIPT_DEVICE_DATA)
	@echo $(MSG_GEN)
	$(Q)$(CPP) $(LINKER_SCRIPT_CPPFLAGS) -P -E $< -o $@

# -------------------------------------------------------------------------------


ELF_LD_FLAGS += -Wl,--entry=reset_handler \
				 -fno-exceptions \
				 -ffunction-sections \
				 -fdata-sections \
				 -fno-asynchronous-unwind-tables \
				 -nostartfiles \
				 -specs=nosys.specs \
				 -mcpu=cortex-m3 -mthumb -O3

CPPFLAGS += \
			-O3 -mcpu=cortex-m3 -mthumb \
			$(shell $(DIR_OPENCM)/scripts/genlink.py $(LINKER_SCRIPT_DEVICE_DATA) $(DEVICE) CPPFLAGS)

CFLAGS += \
		  -ffunction-sections \
		  -fdata-sections \
		  -fno-common \
		  -fno-asynchronous-unwind-tables

INCLUDES += \
			$(DIR_OPENCM)/include

prepare: $(DIR_OPENCM)

$(DIR_OPENCM):
	@echo $(MSG_PREPARE)
	$(Q)unzip $(OPENCM_DIST) -d $(dir $(DIR_OPENCM))
	$(Q)make -C $(DIR_OPENCM) CC=$(CC) AR=$(AR) STANDARD_FLAGS+="$(SYSTEM_CPPFLAGS) -Wno-everything" TARGETS="$(OPENCM_TARGET)"
