
# TARGET = arm-none-eabi-clang
#
# Special targets:
# * prebuilt-toolchain

LLVM_PACKAGE = LLVMEmbeddedToolchainForArm-0.1
LLVM_DIST = $(DIR_DIST)/$(LLVM_PACKAGE)-src.zip
DIR_LLVM = $(DIR_EXT)/LLVM-embedded-toolchain-for-Arm-main

DIR_LLVM_BUILD = $(DIR_OUT)/clang
DIR_TOOLCHAIN = $(DIR_OUT)/$(TARGET)

TOOLCHAIN_ARCHIVE = $(error TOOLCHAIN_ARCHIVE variable is not set)

# -------------------------------------------------------------
#  Toolchain preparation
# -------------------------------------------------------------

all: $(DIR_TOOLCHAIN)

$(DIR_LLVM): $(LLVM_DIST)
	@echo $(MSG_PREPARE)
	$(Q)mkdir -p $(shell dirname $(DIR_LLVM))
	$(Q)unzip -Dfq $(LLVM_DIST) -d $(shell dirname $(DIR_LLVM))

$(DIR_LLVM_BUILD): $(DIR_LLVM)
	@echo $(MSG_PREPARE)

$(DIR_TOOLCHAIN): $(DIR_LLVM) $(DIR_LLVM_BUILD)
	@echo $(MSG_PREPARE)
	$(Q)cd $(DIR_LLVM) && \
		./setup.sh && \
		. ./venv/bin/activate && \
		build.py \
			--install-dir $(DIR_TOOLCHAIN) \
			--build-dir $(DIR_LLVM_BUILD) \
			prepare configure clang newlib compiler-rt libcxx

build-toolchain: $(DIR_LLVM)
.PHONY: build-toolchain

# -------------------------------------------------------------
#  Pre-build toolchains
# -------------------------------------------------------------

prebuilt-toolchain:
	@echo $(MSG_PREPARE)
	$(Q)mkdir -p $(DIR_TOOLCHAIN)
	$(Q)tar xf $(TOOLCHAIN_ARCHIVE) --strip-components 1 -C $(DIR_TOOLCHAIN) --mtime=$(DIR_TOOLCHAIN)

.PHONY: prebuilt-toolchain

# -------------------------------------------------------------
#  Compiler variables
# -------------------------------------------------------------

PREFIX = $(DIR_TOOLCHAIN)

AR = $(DIR_TOOLCHAIN)/bin/llvm-ar
AS = $(DIR_TOOLCHAIN)/bin/clang -xassember
CC = $(DIR_TOOLCHAIN)/bin/clang
CXX = $(DIR_TOOLCHAIN)/bin/clang++

LD = $(DIR_TOOLCHAIN)/bin/clang
LD_CC = $(DIR_TOOLCHAIN)/bin/clang
LD_CXX = $(DIR_TOOLCHAIN)/bin/clang++

RANLIB = $(DIR_TOOLCHAIN)/bin/llvm-ranlib
OBJCOPY = $(DIR_TOOLCHAIN)/bin/llvm-objcopy
OBJDUMP = $(DIR_TOOLCHAIN)/bin/llvm-objdump
STRIP = $(DIR_TOOLCHAIN)/bin/llvm-strip

LLVM_RUNTIME ?= $(error LLVM_RUNTIME not defined in variant configuration)

SYSTEM_CPPFLAGS += \
				   -I$(DIR_TOOLCHAIN)/lib/clang-runtimes/$(LLVM_RUNTIME)/include \
				   --config $(DIR_TOOLCHAIN)/bin/$(LLVM_RUNTIME)_nosys.cfg

SYSTEM_LDFLAGS += \
				  -L$(DIR_TOOLCHAIN)/lib/clang-runtimes/$(LLVM_RUNTIME)/lib \
				  --config $(DIR_TOOLCHAIN)/bin/$(LLVM_RUNTIME)_nosys.cfg \
				  -Wno-unused-command-line-argument

WARNING_FLAGS += -Wno-main-return-type 

CFLAGS += $(WARNING_FLAGS)
CXXFLAGS += $(WARNING_FLAGS)
CPPFLAGS += $(SYSTEM_CPPFLAGS)
LDFLAGS += $(SYSTEM_LDFLAGS)
