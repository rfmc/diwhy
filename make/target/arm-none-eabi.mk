
# TARGET = arm-none-eabi
#
# Special targets:
# * prebuild-toolchain

GCC_PACKAGE = gcc-arm-none-eabi-10-2020-q4-major
GCC_DIST = $(DIR_DIST)/$(GCC_PACKAGE)-src.tar.bz2
DIR_GCC = $(DIR_EXT)/$(GCC_PACKAGE)

DIR_GCC_BUILD = $(DIR_OUT)/gcc
DIR_TOOLCHAIN = $(DIR_OUT)/$(TARGET)

TOOLCHAIN_ARCHIVE = $(error TOOLCHAIN_ARCHIVE variable is not set)

# -------------------------------------------------------------
#  Toolchain preparation
# -------------------------------------------------------------

all: $(DIR_TOOLCHAIN)

$(DIR_GCC): $(GCC_DIST)
	@echo $(MSG_PREPARE)
	$(Q)mkdir -p $(shell dirname $(DIR_GCC))
	$(Q)tar xjf $(GCC_DIST) -C $(shell dirname $(DIR_GCC))

$(DIR_GCC_BUILD): $(DIR_GCC)
	@echo $(MSG_PREPARE)

$(DIR_TOOLCHAIN): $(DIR_GCC) $(DIR_GCC_BUILD)
	# 
	# FIXME FIXME
	#
	@echo $(MSG_PREPARE)
	$(Q)cd $(DIR_GCC_BUILD) && \
		./build-prerequisites.sh --skip_steps=mingw32 && \
		./build-toolchain.sh --build_type=native --skip_steps=manual,mingw32,mingw32-gdb-with-python

build-toolchain: $(DIR_GCC)
.PHONY: build-toolchain

# -------------------------------------------------------------
#  Pre-built toolchains
# -------------------------------------------------------------

prebuilt-toolchain:
	@echo $(MSG_PREPARE)
	$(Q)mkdir -p $(DIR_TOOLCHAIN) && \
		cd $(DIR_TOOLCHAIN) && \
		tar xf $(TOOLCHAIN_ARCHIVE) --strip-components 1

.PHONY: prebuilt-toolchain

# -------------------------------------------------------------
#  Compiler variables
# -------------------------------------------------------------

PREFIX = $(DIR_TOOLCHAIN)

AR = $(DIR_TOOLCHAIN)/bin/arm-none-eabi-ar
AS = $(DIR_TOOLCHAIN)/bin/arm-none-eabi-as
CC = $(DIR_TOOLCHAIN)/bin/arm-none-eabi-gcc
CXX = $(DIR_TOOLCHAIN)/bin/arm-none-eabi-g++

LD_CC = $(DIR_TOOLCHAIN)/bin/arm-none-eabi-gcc
LD_CXX = $(DIR_TOOLCHAIN)/bin/arm-none-eabi-g++

RANLIB = $(DIR_TOOLCHAIN)/bin/arm-none-eabi-ranlib
OBJCOPY = $(DIR_TOOLCHAIN)/bin/arm-none-eabi-objcopy
OBJDUMP = $(DIR_TOOLCHAIN)/bin/arm-none-eabi-objdump
STRIP = $(DIR_TOOLCHAIN)/bin/arm-none-eabi-strip

# SYSTEM_CPPFLAGS - empty
# SYSTEM_LDFLAGS - empty
