
ifeq ($(V),1)
    Q := 
else
    Q := @
endif

MSG_PREPARE     = " PREPARE   $@"
MSG_CP          = " CP        $@"
MSG_GEN         = " GEN       $@"

MSG_AR          = " AR        $@"
MSG_AS          = " AS        $@"
MSG_CC          = " CC        $@"
MSG_LD          = " LD        $@"
MSG_LD_ELF      = " LD [ELF]  $@"
MSG_CXX         = " CXX       $@"
MSG_OBJCOPY     = " OBJCOPY   $@"
MSG_SYMS        = " SYMS      $@"
MSG_OBJDUMP     = " OBJDUMP   $@"
MSG_LN          = " LN        $@"
MSG_MKDIR       = " MKDIR     $@"


MSG_CC_PLATFORM = " CC [SDK]  $@"
MSG_AS_PLATFORM = " AS [SDK]  $@"
